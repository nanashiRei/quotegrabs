<?php

class Irc_View_Helper_HighlightNick extends Zend_View_Helper_Abstract
{

    public function highlightNick($message, $nick)
    {
        return str_replace($nick, '<span class="highlight">'.$nick.'</span>', $message);
    }
    
}