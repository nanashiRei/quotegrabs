<?php
class Irc_View_Helper_IrcMask extends Zend_View_Helper_Abstract
{
    private $_nick;
    private $_ident;
    private $_host;
    
    const HOSTMASK_REGEX = '/^([^!]+)!([a-z0-9]+)@(.*)$/i';
    
    public function ircMask($mask = null) 
    {
        if(is_string($mask))
        {
            if(preg_match(self::HOSTMASK_REGEX,$mask,$match))
            {
                $this->_nick = $match[1];
                $this->_ident = $match[2];
                $this->_host = $match[3];
            }
        }
        return $this;
    }
    
	/**
     * @return the $_nick
     */
    public function getNick ()
    {
        return $this->_nick;
    }

	/**
     * @param field_type $_nick
     */
    public function setNick ($_nick)
    {
        $this->_nick = $_nick;
        return $this;
    }

	/**
     * @return the $_ident
     */
    public function getIdent ()
    {
        return $this->_ident;
    }

	/**
     * @param field_type $_ident
     */
    public function setIdent ($_ident)
    {
        $this->_ident = $_ident;
        return $this;
    }

	/**
     * @return the $_host
     */
    public function getHost ()
    {
        return $this->_host;
    }

	/**
     * @param field_type $_host
     */
    public function setHost ($_host)
    {
        $this->_host = $_host;
        return $this;
    }

}

?>