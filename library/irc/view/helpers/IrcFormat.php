<?php

class Irc_View_Helper_IrcFormat extends Zend_View_Helper_Abstract
{
    private $_messageRaw;
    private $_messageStripped;
    private $_messageFormatted;
    
    const COLOR = "\x03";
    const UNDERLINE = "\x1F";
    const ITALIC =  "\x1D";
    const BOLD =  "\x02";
    
    public function ircFormat($message)
    {
        $this->_messageRaw = $message;
        $this->_parse();
        return $this;
    }
    
    private function _parse()
    {
        $this->_messageFormatted = '';
        $this->_messageStripped = '';
        $colorTag = false;
        $underlineOpen = false;
        $italicOpen = false;
        $boldOpen = false;
        $styleBuffer = array();
        for($pos = 0; $pos < strlen($this->_messageRaw); $pos++)
        {
            switch($this->_messageRaw[$pos])
            {
                case self::COLOR:
                    $colorTag = true;
                    break;
                case self::UNDERLINE:
                    if($underlineOpen == false)
                    {
                        $this->_messageFormatted .= '<span style="text-decoration: underline;">';
                        $underlineOpen = true;
                    }
                    else
                    {
                        $this->_messageFormatted .= '</span>';
                        $underlineOpen = false;
                    }
                    break;
                case self::ITALIC:
                    if($italicOpen == false)
                    {
                        $this->_messageFormatted .= '<span style="font-style: italic;">';
                        $italicOpen = true;
                    }
                    else 
                    {
                        $this->_messageFormatted .= '</span>';
                        $italicOpen = false;
                    }
                    break;
                case self::BOLD:
                    if($boldOpen == false)
                    {
                        $this->_messageFormatted .= '<span style="font-weight: bold;">';
                        $boldOpen = true;
                    }
                    else
                    {
                        $this->_messageFormatted .= '</span>';
                        $boldOpen = false;
                    }
                    break;
                default:
                    if($colorTag == true && (is_numeric($this->_messageRaw[$pos]) || $this->_messageRaw[$pos] == ','))
                    {
                        // Do nothing
                    }
                    else
                    {
                        $this->_messageFormatted .= $this->_messageRaw[$pos];
                        $this->_messageStripped .= $this->_messageRaw[$pos];
                        $colorTag = false;
                    }
                    break;
            }
        }
        if($underlineOpen == true) $this->_messageFormatted .= '</span>';
        if($italicOpen == true) $this->_messageFormatted .= '</span>';
        if($boldOpen == true) $this->_messageFormatted .= '</span>';
    }
    
	/**
     * @return the $_messageRaw
     */
    public function getMessageRaw ()
    {
        return $this->_messageRaw;
    }

	/**
     * @param field_type $_messageRaw
     */
    public function setMessageRaw ($_messageRaw)
    {
        $this->_messageRaw = $_messageRaw;
        return $this;
    }

	/**
     * @return the $_messageStripped
     */
    public function getMessageStripped ()
    {
        return $this->_messageStripped;
    }

	/**
     * @param field_type $_messageStripped
     */
    public function setMessageStripped ($_messageStripped)
    {
        $this->_messageStripped = $_messageStripped;
        return $this;
    }

	/**
     * @return the $_messageFormatted
     */
    public function getMessageFormatted ()
    {
        return $this->_messageFormatted;
    }

	/**
     * @param string $_messageFormatted
     */
    public function setMessageFormatted ($_messageFormatted)
    {
        $this->_messageFormatted = $_messageFormatted;
        return $this;
    }

}

?>