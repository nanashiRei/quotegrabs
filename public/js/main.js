(function($){
	
	$(document).ready(function(){
		$('table tbody tr').hover(
			function(){ $(this).addClass('tr-hover'); },
			function(){ $(this).removeClass('tr-hover'); }
		);
		
		$('nav li').hover(
			function(){ $(this).addClass('hover'); }, 
			function(){ $(this).removeClass('hover'); }
		);
		
		$('tr.quote-row').click(function(){
			var qid = $(this).attr('id').substr(6);
			location.href = '/quote/' + qid;
		});
		
		$('table tbody tr:nth-child(even)').addClass('row1');
		
		$('.page-select span').buttonset();
		$('.page-select input[type="radio"]').change(function(){
			var page = $(this).attr('id').substr(5);
			location.href = '/overview/' + page;
		});
		
		$('form input[type="submit"]').button();
	});
	
})(jQuery);