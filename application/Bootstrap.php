<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initBaseUrl ()
    {
        // Need to fix this
        $this->bootstrap('view');
        $view = $this->getResource('view');
        if (substr_count($_SERVER['HTTP_HOST'], 'my.phpcloud') > 0) {
            $view->getHelper('BaseUrl')->setBaseUrl('/QuoteGrabs');
        } else {
            $view->getHelper('BaseUrl')->setBaseUrl('/');
        }
    }

    protected function _initViewHelper ()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->partial()->setObjectKey('model');
        $view->partialLoop()->setObjectKey('model');
    }

    protected function _initAuth()
    {
        $auth = Zend_Auth::getInstance();
        $this->bootstrap('view');
        $view = $this->getResource('view');
        if($auth->hasIdentity())
        {
            $view->loggedIn = true;
            $view->username = $auth->getIdentity()->username;
        }
        else
        {
            $view->loggedIn = false;
        }   
    }
    
    protected function _initHead ()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        
        $view->headTitle('QuoteGrabs')
            ->setSeparator(' - ')
            ->setDefaultAttachOrder(
                Zend_View_Helper_Placeholder_Container::PREPEND);
        
        $view->headLink()
            ->appendStylesheet($view->baseUrl('css/quotegrabs.main.css'))
            ->appendStylesheet(
                $view->baseUrl('css/ui-darkness/jquery-ui-1.8.19.custom.css'))
            ->headLink(
                array('rel' => 'shortcut icon', 
                        'href' => $view->baseUrl('images/comments.png'), 
                        'type' => 'image/png'), 'PREPEND');
        
        $view->headScript()
            ->appendFile(
                '//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js')
            ->appendFile(
                '//ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js')
            ->appendFile($view->baseUrl('js/main.js'));
    }

    protected function _initRoutes ()
    {
        $this->bootstrap('frontController');
        $router = $this->getResource('frontController')->getRouter();
        $adminRoute = new Zend_Controller_Router_Route('admin/:action', 
                array('module' => 'admin', 'controller' => 'index', 
                        'action' => 'index'));
        $overviewRoute = new Zend_Controller_Router_Route('overview/:page', 
                array('module' => 'default', 'controller' => 'index', 
                        'action' => 'index', 'page' => 1));
        $singleRoute = new Zend_Controller_Router_Route('quote/:quoteid', 
                array('module' => 'default', 'controller' => 'index', 
                        'action' => 'quote'));
        $randomRoute = new Zend_Controller_Router_Route('quote/random', 
                array('module' => 'default', 'controller' => 'index', 
                        'action' => 'random'));
        $loginRoute = new Zend_Controller_Router_Route('login/:action', 
                array('module' => 'admin', 'controller' => 'auth', 
                        'action' => 'index'));
        $router->addRoute('admin_route', $adminRoute)
            ->addRoute('login_route', $loginRoute)
            ->addRoute('overview_route', $overviewRoute)
            ->addRoute('single_route', $singleRoute)
            ->addRoute('random_route', $randomRoute);
    }
}

