<?php

class Admin_IndexController extends Zend_Controller_Action
{

    public function init ()
    {
        $this->view->documentClasses = array();
    }

    public function indexAction ()
    {
        $this->_checkAuth();
        $this->_forward('edit');
    }

    public function editAction ()
    {
        // action body
        $this->_checkAuth();
        $this->view->action = 'Administration';
        $this->view->documentClasses[] = 'admin';
    }
    
    protected function _checkAuth()
    {
        $auth = Zend_Auth::getInstance();
        if (!$auth->hasIdentity())
        {
            $this->_forward('index','auth','admin');
        }
    }

}

