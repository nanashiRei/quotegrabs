<?php

class IndexController extends Zend_Controller_Action
{
    
    protected $_quoteGrabsDb = null;
    protected $_quoteGrabsConfig = null;

    public function init ()
    {
        $this->view->documentClasses = array();
        $this->_quoteGrabsConfig = new Zend_Config_Ini(
                APPLICATION_PATH . '/configs/quotegrabs.ini', APPLICATION_ENV);
        $this->_quoteGrabsDb = Zend_Db::factory($this->_quoteGrabsConfig->database->adapter,$this->_quoteGrabsConfig->database->params);
    }

    public function indexAction ()
    {
        $dbTable = new Application_Model_DbTable_Quotegrabs(array('db' => $this->_quoteGrabsDb));
        $quotesMapper = new Application_Model_QuotegrabMapper();
        $quotesMapper->setDbTable($dbTable);
        
        $count = (int) $this->_quoteGrabsConfig->overview->settings->entriesPerPage;
        $this->view->page = max(1, (int) $this->getRequest()->getParam('page', 1));
        $pageOffset = ($this->view->page - 1) * $count;
        $this->view->pages = $quotesMapper->getDbTable()->getCount() / $count;
        
        $quotes = $quotesMapper->fetchNewToOld($count, $pageOffset);
        
        $this->view->headTitle('Übersicht');
        
        $this->view->documentClasses[] = 'overview';
        $this->view->action = 'Übersicht';
        $this->view->quotes = $quotes;
    }

    public function quoteAction ()
    {
        $dbTable = new Application_Model_DbTable_Quotegrabs(array('db' => $this->_quoteGrabsDb));
        $quotesMapper = new Application_Model_QuotegrabMapper();
        $quotesMapper->setDbTable($dbTable);
        
        $quoteId = $this->_request->getParam('quoteid');
        $quote = new Application_Model_Quotegrab();
        $quotesMapper->find($quoteId, $quote);
        $this->view->quote = $quote;
        
        $title = $this->view->ircFormat($quote->getQuote())
            ->getMessageStripped();
        if (strlen($title) > 35)
            $title = substr($title, 0, 35) . '...';
        $this->view->headTitle($title);
        
        $this->view->documentClasses[] = 'single-quote';
        $this->view->action = 'Quote #' . $quote->getId();
    }

    public function randomAction ()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        
        $dbTable = new Application_Model_DbTable_Quotegrabs(array('db' => $this->_quoteGrabsDb));
        $quotesMapper = new Application_Model_QuotegrabMapper();
        $quotesMapper->setDbTable($dbTable);
        
        $random = new Application_Model_Quotegrab();
        $quotesMapper->fetchRandom($random);
        
        $this->_helper->getHelper('Redirector')->gotoRoute(
                array('quoteid' => $random->getId()), 'single_route');
    }

}





