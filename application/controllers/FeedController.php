<?php

class FeedController extends Zend_Controller_Action
{
    protected $_feed;
    protected $_quoteGrabsDb = null;
    protected $_quoteGrabsConfig = null;
    
    public function init()
    {
        $this->_quoteGrabsConfig = new Zend_Config_Ini(
                APPLICATION_PATH . '/configs/quotegrabs.ini', APPLICATION_ENV);
        $this->_quoteGrabsDb = Zend_Db::factory($this->_quoteGrabsConfig->database->adapter,$this->_quoteGrabsConfig->database->params);
        
        /**
         * Create the parent feed
         */
        $this->_feed = new Zend_Feed_Writer_Feed;
        $this->_feed->setTitle('QuoteGrabs Random Quote Feed');
        $this->_feed->setLink('http://quotes.bakashimoe.me');
        $this->_feed->setFeedLink('http://quotes.bakashimoe.me/feed/atom', 'atom');
        $this->_feed->addAuthor(array(
                'name'  => 'QuoteGrabs',
                'email' => 'quotes@bakashimoe.me',
                'uri'   => 'http://quotes.bakashimoe.me',
        ));
        $this->_feed->setDateModified(time());
    }

    public function indexAction()
    {
        // action body
    }

    public function atomAction()
    {
        //$this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        $dbTable = new Application_Model_DbTable_Quotegrabs(array('db' => $this->_quoteGrabsDb));
        $quotesMapper = new Application_Model_QuotegrabMapper();
        $quotesMapper->setDbTable($dbTable);
        $quotes = $quotesMapper->fetchNewToOld(25,0);
        
        foreach($quotes as $quote) {
            $entry = $this->_feed->createEntry();
            $entry->setTitle(htmlspecialchars($this->view->ircFormat($quote->getQuote())->getMessageStripped()));
            $entry->setLink('http://quotes.bakashimoe.me/quote/'.$quote->getId());
            $entry->addAuthor(array(
                    'name'  => $quote->getNick(),
                    'email' => $quote->getNick().'@users.bakashimoe.me',
                    'uri'   => 'http://bakashimoe.me/index.php?page=MembersList',
            ));
            $entry->setDateModified($quote->getAddedAt());
            $entry->setDateCreated($quote->getAddedAt());
            $entry->setDescription('Random Quote #'.$quote->getId());
            $entry->setContent('Empty');
            $this->_feed->addEntry($entry);
        }
        
        $this->view->feed = $this->_feed->export('atom');
    }
}

