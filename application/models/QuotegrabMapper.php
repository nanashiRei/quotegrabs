<?php

class Application_Model_QuotegrabMapper
{
    protected $_dbTable;
    
    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Quotegrabs');
        }
        return $this->_dbTable;
    }
    
    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Ungueltige Gateway-Klasse fuer Tabellendaten');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    
    public function save(Application_Model_Quotegrab $message) {
        $data = array(
                'id'		=> $message->getId(),
                'nick'	    => $message->getUserId(),
                'hostmask'	=> $message->getMessage(),
                'added_by'  => $message->getAddedBy(),
                'added_at'  => $message->getAddedAt(),
                'quote'     => $message->getQuote()
        );
        if (null === $message->getId()) {
            unset($data['id']);
            $data['added_at'] = time();
            $message->setId($this->getDbTable()->insert($data));
        }
        else {
            $this->getDbTable()->update($data, array("_id = ?" => $data['id']));
        }
        return $this;
    }
    
    public function delete($id) {
        $result = $this->getDbTable()->delete(array('_id = ?' => $id));
        if($result === false)
            return false;
        return true;
    }
    
    public function find($id, Application_Model_Quotegrab &$message) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return null;
        }
        $row = $result->current();
        $message->setId($row->id)
        ->setNick($row->nick)
        ->setHostmask($row->hostmask)
        ->setAddedBy($row->added_by)
        ->setAddedAt($row->added_at)
        ->setQuote($row->quote);
    }
    
    public function fetchRandom(Application_Model_Quotegrab &$message) {
        $resultSet = $this->fetchAll();
        $max_id = 0;
        foreach($resultSet as $row) {
            $max_id = max($max_id,$row->id);
        }
        $randomResult = false;
        while($randomResult === false || count($randomResult) == 0){
            $randomResult = $this->getDbTable()->find(mt_rand(0,$max_id));
        }
        $row = $randomResult->current();
        $message = new Application_Model_Quotegrab();
        $message->setId($row->id)
              ->setNick($row->nick)
              ->setHostmask($row->hostmask)
              ->setAddedBy($row->added_by)
              ->setAddedAt($row->added_at)
              ->setQuote($row->quote);
    }
    
    public function fetchNewToOld($count=50,$offset=0)
    {
        return $this->fetchAll(null,'added_at DESC',$count,$offset);
    }
    
    public function fetchAll($where = null, $order = null, $count = null, $offset = null) {
        $resultSet = $this->getDbTable()->fetchAll($where,$order,$count,$offset);
        $entries = array();
        foreach ($resultSet as $row) {
            $entry = new Application_Model_Quotegrab();
            $entry->setId($row->id)
            ->setNick($row->nick)
            ->setHostmask($row->hostmask)
            ->setAddedBy($row->added_by)
            ->setAddedAt($row->added_at)
            ->setQuote($row->quote);
            $entries[] = $entry;
        }
        return $entries;
    }    
}

