<?php

class Application_Model_Quotegrab
{

    private $_id;

    private $_nick;

    private $_hostmask;

    private $_addedBy;

    private $_addedAt;

    private $_quote;

    private $_mapper;

    public function __construct (array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function setOptions (array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
    }

    public function __get ($name)
    {
        $method = 'get' . ucfirst($name);
        if ('mapper' == $name || ! method_exists($this, $method)) {
            throw new Exception(
                    "Ungueltige Getter-Methode fuer Eigenschaft '$method'");
        }
        return $this->$method();
    }

    public function __set ($name, $value)
    {
        $method = 'set' . ucfirst($name);
        if ('mapper' == $name || ! method_exists($this, $method)) {
            throw new Exception(
                    "Ungueltige Setter-Methode fuer Eigenschaft '$name'");
        }
        return $this->$method($value);
    }

    public function getMapper ()
    {
        if ($this->_mapper === null) {
            $this->_mapper = new Application_Model_QuoteGrabMapper();
        }
        return $this->_mapper;
    }

    public function setMapper ($mapper)
    {
        $this->_mapper = $mapper;
        return $this;
    }

    /**
     *
     * @return the $_id
     */
    public function getId ()
    {
        return $this->_id;
    }

    /**
     *
     * @param field_type $_id            
     */
    public function setId ($_id)
    {
        $this->_id = $_id;
        return $this;
    }

    /**
     *
     * @return the $_nick
     */
    public function getNick ()
    {
        return $this->_nick;
    }

    /**
     *
     * @param field_type $_nick            
     */
    public function setNick ($_nick)
    {
        $this->_nick = $_nick;
        return $this;
    }

    /**
     *
     * @return the $_hostmask
     */
    public function getHostmask ()
    {
        return $this->_hostmask;
    }

    /**
     *
     * @param field_type $_hostmask            
     */
    public function setHostmask ($_hostmask)
    {
        $this->_hostmask = $_hostmask;
        return $this;
    }

    /**
     *
     * @return the $_addedBy
     */
    public function getAddedBy ()
    {
        return $this->_addedBy;
    }

    /**
     *
     * @param field_type $_addedBy            
     */
    public function setAddedBy ($_addedBy)
    {
        $this->_addedBy = $_addedBy;
        return $this;
    }

    /**
     *
     * @return the $_addedAt
     */
    public function getAddedAt ()
    {
        return $this->_addedAt;
    }

    /**
     *
     * @param field_type $_addedAt            
     */
    public function setAddedAt ($_addedAt)
    {
        $this->_addedAt = $_addedAt;
        return $this;
    }

    /**
     *
     * @return the $_quote
     */
    public function getQuote ()
    {
        return $this->_quote;
    }

    /**
     *
     * @param field_type $_quote            
     */
    public function setQuote ($_quote)
    {
        // For the love of god, make it UTF-8!
        if (! mb_check_encoding($_quote, 'utf-8'))
            $_quote = mb_convert_encoding($_quote, 'utf-8');
        
        $this->_quote = $_quote;
        return $this;
    }

}

